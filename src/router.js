import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
	mode: "history",
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/payments-data',
			name: 'payments-data',
			component() { return import("./views/Payments.vue") }
		},
		{
			path: '/quiz',
			name: 'quiz',
			component() { return import("./views/QuizSection.vue") }
		}
	]
})
