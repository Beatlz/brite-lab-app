// Require firebase
const firebase = require("firebase");
// Initialize firebase
var config = {
    apiKey: "AIzaSyAywocKlDYS2ZpfJY6a7B-FwmrGfSxdE2E",
    authDomain: "brite-core-app.firebaseapp.com",
    databaseURL: "https://brite-core-app.firebaseio.com",
    projectId: "brite-core-app",
    storageBucket: "brite-core-app.appspot.com",
    messagingSenderId: "365313863708"
  };
firebase.initializeApp(config);
// Import Vue
import Vue from 'vue';
import Vuex from 'vuex';
// Use vue
Vue.use(Vuex);
// Vuex constructor
export default new Vuex.Store({
	state: {
		loadingStatus: false,
		postingStatus: false,
		paymentsList: [],
		descriptionChanges: [],
		descId: "",
		desc: ""
	},
	mutations: {
		setLoadingStatus(state, status) {
			state.loadingStatus = status;
		},
		setPostingStatus(state, status) {
			state.postingStatus = status;
		},
		setPayments(state, data) {
			state.paymentsList = data;
		},
		setDescription(state, data) {
			state.descriptionChanges.push({
				id: data.id,
				mod: data.description
			});
		}
	},
	actions: {
		async getPayments(context) {
			try {
				// Start loading
				context.commit("setLoadingStatus", true);
				// Call firebase
				const payments = await firebase.database().ref("/payments/").once("value");
				// Convert to array
				let paymentsList = Object.values(payments.val());
				// Data ready
				context.commit("setLoadingStatus", false);
				// Commit data
				context.commit("setPayments", paymentsList);
			} catch(err) {
				console.error(err);
			}
		},
		async postDescription({ commit, state }, changeData) {
			try {
				console.log(changeData);
				// Start posting
				commit("setPostingStatus", true);
				// Post to firebase
				await firebase.database().ref("/payments/" + changeData.id).update({ Description: changeData.description });
				// Successful change
				commit("setPostingStatus", false);
				// Commit data
				commit("setDescription", changeData);
			} catch(err) {
				console.error(err);
			}
		}
	}
})
