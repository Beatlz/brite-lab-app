# brite-core-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Deployed site
[Click here to see the payments table test in action!](https://brite-core-app.firebaseapp.com/)

### How long did you spend on the test? Would you do anything differently?
I spent about 3 to 5 minutes on the two quiz questions, and about 4 to 5 hours on the coding test. I spent most of the time styling, because I didn't use any library to aid my code, and I decided to build the grid from scratch. If I had a little more time, I would spend a lot testing my UI/UX with real time users, because, although I've gathered a lot of experience in the matter, no one makes it on the first jump. I spent no time on security, not because I don't think it's important, but because I didn't want to confuse you by making accounts and handling them to you… plus you can handle that with firebase almost entirely.

### In what ways would you adapt your component so that it could be used in many different scenarios where a data table is required?
One word: responsiveness. I did not take the time to make this fully responsive. I work with big data tables a lot, and it's no secret that mobile and big numbers in tables DO NOT get along. If I had a little more time, I'd build a component that turns rows into cards. I did spend a bit of time on the search functionality, thinking about that scenario. That being said, I would add a prop that takes in a data collection array. I know it wouldn't take that long to do that for this test, but when you take into account that one of the requirements was to handle DB persistence, that changes a bit.

### What is your favorite CSS property? Why?
Oh, boy… that's a tough question. I've been working with grid displays a lot lately, they make things much easier. Actually, for this test, I chose to use css grid rather than a ```<table/>``` component. Why? Well, I love that it takes into account a second dimension. I knonw flex KINDA does that, but not fully. I don't think I love ONE unique CSS property, but rather what you can do when you know how to combine them. You can make pretty cool animations with it, which is awesome considering how little resources it takes from your device to run them.

### What is your favorite modern Javascript feature? Why?
Same as with CSS, I really don't have a favourite. I think async/await, { destructures }, .map/.filter are my most used modern features. Why? Well, async/await's are just amazing for readability, compared to the infamous "callback hells". Destructures are just beautiful code, same with .map/.filter's, the good old for loops are just not as beautiful as those. 

### What is your favorite third-party Vue.js library? Why?
To be honest, I really don't use a lot of third party Vue.js libraries. I believe they take more from me than what they give. I think it's way more benefitial to take the time to code everything yourself. I'm not against libraries, not even against UI libraries, I use UIKit A LOT. Combining awesome JS libraries like Fuse.js, moment.js, lodash.js, three.js with UIKit and VueJS makes you virtually unstoppable. I guess if I had to pick one, I'd say the vue-lottie integration, lottie is amazing. If I had to pick a UI Vue library, I'd go with Vuetify, because of how well it works. 