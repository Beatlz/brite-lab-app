let Fernet = require('fernet'),
    secret = new Fernet.Secret('TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM=');
    
// First question
var test = [ 104, 116, 116, 112, 115, 58, 47, 47, 101, 110, 103, 105, 110, 101, 101, 114, 105, 110, 103, 45, 97, 112, 112, 108, 105, 99, 97, 116, 105, 111, 110, 46, 98, 114, 105, 116, 101, 99, 111, 114, 101, 46, 99, 111, 109, 47, 113, 117, 105, 122, 47, 115, 97, 97, 115, 100, 97, 115, 100, 108, 102, 108, 102, 108, 115 ];
var solution = test.map((e, index) => {
  return String.fromCharCode(e);
});
// result
console.log(solution.join(""));

// Second question

// Oh no! The code is going over the edge! What are you going to do?
let message = 'gAAAAABcYDWO6cs8oPgta5nuzJPl2iVuGXD4wUPGg1mlBr4XHk9J1H_bSY123ATS5TXRYKA11IFWxs2pgjjOxVj3ZZV6AjSinzeZT1w-aO6JmHWsrOJBkeqRpTSctOvJhRTk_X7LqK0YSnM7XIkIo1d5vme2Qv_6zUg3RxCMh3YdXXt-IwPYTBWuP4GJwWCU99dvF1xLOXvo',
    token = new Fernet.Token({ secret, token: message, ttl:0 });
// result
console.log(token.decode());